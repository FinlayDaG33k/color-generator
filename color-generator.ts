import { hexToHsl } from "./src/hex-to-hsl.ts";
import { generateColor } from "./src/generate-color.ts";
import { hslToHex } from "./src/hsl-to-hex.ts";
import { template, Palette } from "./src/template.ts";

/**
 * Base color distances.
 * These colors are calculated from base color #00408f.
 */
const bases = {
  'primary-light': { h: 0, s: 0, l: 12}, // #005ccc
  'primary-dark': { h: 0, s: 0, l: -14}, // #002047
  'background-light': { h: -2, s: 0, l: -22}, // #000f1f
  'background-medium': { h: -3, s: 0, l: -24}, // #000a14
  'background-dark': { h: -3, s: 0, l: -26}, // #00050a
};

// Get the picked color from the arguments
// Exit if none specified
const picked = Deno.args[0];
if(!picked) {
  console.error('Please specify a hex color! (see README.md for instructions)');
  Deno.exit(1);
}

// Turn our picked color to HSL
const pickedHsl = hexToHsl(picked);

// Generate our palette
const palette: Palette = {
  'primary-light': '',
  'primary-normal': picked.toLowerCase(),
  'primary-dark': '',
  'background-light': '',
  'background-medium': '',
  'background-dark': '',
};
for(const [name, distances] of Object.entries(bases)) {
  const hsl = generateColor(pickedHsl, distances);
  //@ts-ignore Must be of string-type either way
  palette[name] = hslToHex(hsl);
}

// Create our "root" containing the color scheme
let root = ':root {\r\n';
for(const [name, hex] of Object.entries(palette)) {
  root += `  --${name}: ${hex};\r\n`;
}
root += '}';

// Modify and save our template for inspection
const output = template({
  root: root,
  palette: palette
});
await Deno.writeTextFile('preview.html', output);

console.log('Color scheme generated!\r\nPlease check "preview.html" in your browser to see it!');