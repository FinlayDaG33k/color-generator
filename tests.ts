import { assertEquals } from "https://deno.land/std@0.148.0/testing/asserts.ts";
import { hslToHex } from "./src/hsl-to-hex.ts";
import { hexToHsl } from "./src/hex-to-hsl.ts";
import { HSL } from "./src/interfaces/hsl.ts";

Deno.test("hexToHsl", async (t) => {
  await t.step("#00408f", () => {
    const hsl: HSL = hexToHsl('#00408f');
    assertEquals(hsl, { h: 213, s: 100, l: 28});
  });

  await t.step("#005ccc", () => {
    const hsl: HSL = hexToHsl('#005ccc');
    assertEquals(hsl, { h: 213, s: 100, l: 40});
  });

  await t.step("#002047", () => {
    const hsl: HSL = hexToHsl('#002047');
  assertEquals(hsl, { h: 213, s: 100, l: 14});
  });

  await t.step("#000f1f", () => {
    const hsl: HSL = hexToHsl('#000f1f');
    assertEquals(hsl, { h: 211, s: 100, l: 6});
  });

  await t.step("#000a14", () => {
    const hsl: HSL = hexToHsl('#000a14');
    assertEquals(hsl, { h: 210, s: 100, l: 4});
  });

  await t.step("#00050a", () => {
    const hsl: HSL = hexToHsl('#00050a');
    assertEquals(hsl, { h: 210, s: 100, l: 2});
  });
});

Deno.test("hslToHex", async (t) => {
  await t.step("#00408f", () => {
    const hex: string = hslToHex({ h: 213, s: 100, l: 28});
    assertEquals(hex, '#00408f');
  });

  await t.step("#005ccc", () => {
    const hex: string = hslToHex({ h: 213, s: 100, l: 40});
    assertEquals(hex, '#005ccc');
  });

  await t.step("#002047", () => {
    const hex: string = hslToHex({ h: 213, s: 100, l: 14});
    assertEquals(hex, '#002047');
  });

  await t.step("#000f1f", () => {
    const hex: string = hslToHex({ h: 211, s: 100, l: 6});
    assertEquals(hex, '#000f1f');
  });

  await t.step("#000a14", () => {
    const hex: string = hslToHex({ h: 210, s: 100, l: 4});
    assertEquals(hex, '#000a14');
  });

  await t.step("#00050a", () => {
    const hex: string = hslToHex({ h: 210, s: 100, l: 2});
    assertEquals(hex, '#00050a');
  });
});
