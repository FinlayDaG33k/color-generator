import { HSL } from "./interfaces/hsl.ts";

/**
 * Convert HSL values to Hex
 * Source: https://stackoverflow.com/a/44134328
 * 
 * @param input
 * @returns string
 */
export function hslToHex(input: HSL): string {
  input.l /= 100;
  const a = input.s * Math.min(input.l, 1 - input.l) / 100;
  const f = (n: number) => {
    const k = (n + input.h / 30) % 12;
    const color = input.l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
  };
  return `#${f(0)}${f(8)}${f(4)}`;
}