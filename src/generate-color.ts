import { HSL } from "./interfaces/hsl.ts";

/**
 * Generate a color based on the specified distances
 * 
 * @param distances 
 * @returns 
 */
export function generateColor(input: HSL, distances: HSL): HSL {
  return {
    h: input.h + distances.h,
    s: input.s + distances.s,
    l: input.l + distances.l,
  };
}