const templateHtml = `<!DOCTYPE html>
<html>
  <head>
    <style>
      {{ROOT}}

      .color {
        min-height: 16vh;
        height: 16vh;
        text-align: center;
        color: #FFFFFF;
        align-items:center;
        justify-content:center;
        display: flex;
      }
    </style>
  </head>
  <body>
    <div class="color" style="background-color: var(--primary-light);">{{PL}}</div>
    <div class="color" style="background-color: var(--primary-normal);">{{PN}}<br />Base Color</div>
    <div class="color" style="background-color: var(--primary-dark);">{{PD}}</div>
    <div class="color" style="background-color: var(--background-light);">{{BL}}</div>
    <div class="color" style="background-color: var(--background-medium);">{{BM}}</div>
    <div class="color" style="background-color: var(--background-dark);">{{BD}}</div>
  </body>
</html>`

export interface Palette {
  'primary-light': string;
  'primary-normal': string;
  'primary-dark': string;
  'background-light': string;
  'background-medium': string;
  'background-dark': string;
}

export interface TemplateOpts {
  root: string;
  palette: Palette;
}

export function template(opts: TemplateOpts) {
  let template = templateHtml;
  template = template.replace('{{ROOT}}', opts.root);
  template = template.replace('{{PL}}', opts.palette['primary-light']);
  template = template.replace('{{PN}}', opts.palette['primary-normal']);
  template = template.replace('{{PD}}', opts.palette['primary-dark']);
  template = template.replace('{{BL}}', opts.palette['background-light']);
  template = template.replace('{{BM}}', opts.palette['background-medium']);
  template = template.replace('{{BD}}', opts.palette['background-dark']);

  return template;
}