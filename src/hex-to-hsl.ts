import { HSL } from "./interfaces/hsl.ts";

/**
 * Convert Hexadecimal colors to HSL
 * Source: https://stackoverflow.com/questions/46432335/hex-to-hsl-convert-javascript
 * 
 * @param hex Hexadecimal color
 * @returns HSL
 */
 export function hexToHsl(hex: string): HSL {
  // Parse the individual channels
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex.toLowerCase());
  if(!result || result.length < 3) throw Error('Could not parse channels!');
  const r = parseInt(result[1], 16) / 255;
  const g = parseInt(result[2], 16) / 255;
  const b = parseInt(result[3], 16) / 255;

  const max = Math.max(r, g, b),
        min = Math.min(r, g, b);

  let h: number, s: number, l: number = (max + min) / 2;

  if(max == min){
    h = s = 0; // achromatic
  } else {
    const d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch(max) {
        case r: 
          h = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g: 
          h = (b - r) / d + 2;
          break;
        case b: 
          h = (r - g) / d + 4;
          break;
    }
    // @ts-ignore H is already defined
    h /= 6;
  }

  s = s*100;
  s = Math.round(s);
  l = l*100;
  l = Math.round(l);
  // @ts-ignore H is already defined
  h = Math.round(360*h);

  return {
    h: h,
    s: s,
    l: l
  };
}