# Color Generator
Quick and dirty color-scheme generator built in Deno.  
Bases the values off the colorscheme used in my [website](https://gitlab.com/finlaydag33k/website).

## Getting started (binary)
Below the instruction for using this repo using the pre-built binaries.

### Windows
1. Download latest binary from the [releases page](https://gitlab.com/FinlayDaG33k/color-generator/-/releases).
2. Run using `color-generator-win64.exe "#HEX"` (where `#HEX` is your base color)
3. Open `preview.html` in your browser to see generated palette.

### GNU+Linux
1. Download latest binary from the [releases page](https://gitlab.com/FinlayDaG33k/color-generator/-/releases).
2. Run using `./color-generator-linux64 "#HEX"` (where `#HEX` is your base color)
3. Open `preview.html` in your browser to see generated palette.

### MacOS
Use the instructions for running [from source](https://gitlab.com/FinlayDaG33k/color-generator#getting-started-using-source)

## Getting started (using source)
Below the instructions for using this repo from source.

###  Requirements
- Deno 1.15.3
- Git

1. Clone repository
2. Run with `deno --allow-read --allow-write color-generator.ts "#HEX"` (where `#HEX` is your base color)
3. Open `preview.html` in your browser to see generated palette.

## Todo
- [ ] Automatically open preview upon creation